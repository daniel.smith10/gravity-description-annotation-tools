import itertools
import json
import os

import pandas as pd

from datetime import datetime, timedelta


def make_report(file_name: str, days_lag: int = 1) -> pd.DataFrame:
    """
    Make an annotation report of data points that have been annotated with the last n days.

    :param file_name: a file of json data. the file_name param is a string. e.g. 'my_file.json'
    :param days_lag: an integer value that represents the number of days of data to pull from the annotation job
    :return annotation_report: get back a pandas DataFrame of information on labeled data.
    """
    with open(file_name, "r", encoding="utf8") as fstream:
        data = json.loads(fstream.read())

    frame = pd.DataFrame.from_records(data['examples'])
    frame = frame[frame['classifications'].apply(lambda seen_by: len(seen_by)) > 1]
    frame = frame[frame['seen_by'].apply(lambda seen_by: len(seen_by)) > 1]

    # TODO: setup a 24 time lag + a little extra to get to a UTC cutoff
    now = datetime.now()
    lag = now - timedelta(days=days_lag)
    frame['most_recent_timestamp'] = frame.apply(get_most_recent_annotation_timestamp, axis=1)
    frame = frame[frame['most_recent_timestamp'] > lag]
    frame.reset_index(inplace=True, drop=True)

    annotators = get_unique_annotators(frame)
    columns = ['exampleID', 'PBID', 'companyName', 'urlSource', 'snippet'] + annotators
    data_rows = []
    for index in range(frame.shape[0]):
        data_rows.append(make_row(frame.loc[index], annotators))
    annotation_report = pd.DataFrame(columns=columns, data=data_rows)

    dst_directory = os.path.split(file_name)[0]
    dst_file_name = f"{now.year}_{now.month}_{now.day}T{now.hour}_{now.minute}_{now.second}_annotation_report.csv"
    annotation_report.to_csv(os.path.join(dst_directory, dst_file_name), index=False)

    return annotation_report


def get_most_recent_annotation_timestamp(row: pd.Series) -> datetime:
    """
    Parse a data points various annotation timestamps, select the most recent, then convert to a datetime object.

    :param row: a pandas Series object which is a single row from a DataFrame.
    :return date_object: Get back the maximum date as a datetime object.
    """
    classes = row.classifications
    most_recent_date_str = max(
        list(itertools.chain(*[[yy['timestamp'] for yy in xx['classified_by']] for xx in classes])))

    try:
        # e.g. 2023-01-23T22:42:39.138+00:00
        date_object = datetime.strptime(most_recent_date_str, "%Y-%m-%dT%H:%M:%S.%f+00:00")
    except ValueError:
        # e.g. 2023-02-14T12:31:01+00:00
        date_object = datetime.strptime(most_recent_date_str, "%Y-%m-%dT%H:%M:%S+00:00")

    return date_object


def get_pbid(row: pd.Series) -> str:
    """

    :param row: a pandas Series object which is a single row from a DataFrame.
    :return: get back a string representing the datapoint's associated pbid.
    """
    return row.metadata['pbid']


def get_company_name(row: pd.Series) -> str:
    """

    :param row: a pandas Series object which is a single row from a DataFrame.
    :return: get back a string representing the datapoint's associated company name.
    """
    return row.metadata['companyName']


def get_url(row: pd.Series) -> str:
    """

    :param row: a pandas Series object which is a single row from a DataFrame.
    :return: get back a string representing the datapoint's associated url from which it was sourced.
    """
    return row.metadata['urlSource']


def get_unique_annotators(data: pd.DataFrame) -> list:
    """
    Parse a DataFrame and get back the set of unique annotator names that have contributed to the project.

    :param data: a pandas DataFrame of annotation data.
    :return annotators: a list of the annotators names.
    """
    annotators = set()
    for index in range(data.shape[0]):
        seen_by = data.seen_by.loc[index]
        for element in seen_by:
            annotators.add(element['annotator'])

    return sorted(annotators)


def make_row(row: pd.Series, unique_annotators: list) -> list:
    """
    Make a row of data for a polished annotation report.

    :param row: a pd.Series object from a DataFrame.
    :param unique_annotators: the list of unique annotator names.
    :return row: a list of data points. Will later be concatenated into a full annotation report.
    """
    data = [row.example_id, get_pbid(row), get_company_name(row), get_url(row), row.content]
    for annotator in unique_annotators:
        data.append(get_annotator_classes(row.classifications, row.seen_by, annotator))

    return data


def get_annotator_classes(class_list: list, seen_list: list, annotator: str) -> str:
    """
    Get the set of classes that a single annotater has labeled a single data point (this is required because an
    annotator can label a datapoint as one or more labels simultaneously).

    :param class_list: a list object containing one or more dictionaries. the dictionaries contain metadata on
    individual classifications, including who made the classification.
    :param seen_list:  a list object containing one or more dictionaries. the dictionaries contain metadata on
    individuals who have seen a data point.
    :param annotator: a string of the name of the annotator.
    :return: get back a newline concatenated string of the one or more labels applied by an annotator to a datapoint.
    """
    try:
        classifications = "\n".join([element['classname'] for element in class_list if
                          annotator in [xx['annotator'] for xx in element['classified_by']]])

    except TypeError:
        classifications = ""
    if not classifications:
        if annotator not in [seen['annotator'] for seen in seen_list]:
            classifications = "Unseen"
        else:
            classifications = "Skipped"

    return classifications


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="The annotation data input file as JSON")
    parser.add_argument("-l", "--lag", help="The number of days of lag to introduce", default=1, type=int)
    args = parser.parse_args()
    make_report(file_name=args.file, days_lag=args.lag)
