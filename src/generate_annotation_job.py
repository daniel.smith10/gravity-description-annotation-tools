import json
import os

from datetime import datetime

import boto3
import pandas as pd

from pbcommon.dao.gravity_s3_dao import GravityS3Dao
from pb_ml.data_acquisition import DataCollector
from pb_ml.model_configuration import ModelConfig
from typing import Union

BASE_DIR = os.path.split(__file__)[0]
GRAVITY_BUCKET = 'pb-nebula'


class GravityCollector(DataCollector):
    def __init__(self, config: Union[str, ModelConfig]):
        super().__init__(config, True)
        self.query_dir = os.path.join(BASE_DIR, 'queries')
        self._instantiate_grav_dao()

    def _instantiate_grav_dao(self):
        client = boto3.client('s3')
        self.gravity_dao = GravityS3Dao(client, GRAVITY_BUCKET)

    def retrieve_data(self, csv: bool = False):
        ids = self.fetch_company_ids()
        frames = [self.fetch(url) for url in ids.url]
        frame = pd.concat(frames)
        frame = frame[~frame.pbid.isnull()]
        frame.reset_index(inplace=True, drop=True)
        frame.reset_index(inplace=True, drop=True)
        today = datetime.today().strftime('%Y-%m-%d')
        if csv:
            frame.to_csv(os.path.join(self.data_dir, f"{today}_annotation_data.csv"), index=False)
        else:
            json_str = frame.to_json(orient='records')
            parsed = json.loads(json_str)
            with open(os.path.join(self.data_dir, f"{today}_annotation_data.json"), 'w') as fstream:
                fstream.write(json.dumps(parsed, indent=4))

        return frame

    def fetch_company_ids(self):
        with open(os.path.join(self.query_dir, 'company_ids.sql')) as fstream:
            test_job_ids = open(os.path.join(self.data_dir, 'test_job_pbids.txt')).read().splitlines()
            black_list_ids = open(
                os.path.join(self.data_dir, 'black_list_pbids.txt')).read().splitlines() + test_job_ids
            black_list_str = ', '.join([f"'{x}'" for x in black_list_ids])
            query_string = fstream.read().format(black_list_ids=black_list_str)

        company_id_frame = self.execute_query(connection_type='snowflake',
                                              credentials_file_path=self.config.credentials.snowflake,
                                              query_string=query_string,
                                              drop_null_values=False,
                                              to_file_path=os.path.join(BASE_DIR, 'updated_company_ids.csv'))

        return company_id_frame

    def fetch(self, url):
        profile = self.gravity_dao.get_full_profile(url)

        try:
            json_profile = json.loads(profile)
        except TypeError:

            return pd.DataFrame()
        company_name = json_profile['entity']['company name']
        pbid = json_profile['entity']['pbid']

        try:
            rows = [(pbid, company_name,
                     xx['sources'][0]['urlSource'],
                     xx['content']) for xx in json_profile['description']['descriptions']]
        except TypeError:
            return pd.DataFrame()

        df = pd.DataFrame(columns=['pbid', 'companyName', 'urlSource', 'content'], data=rows)

        return df


if __name__ == '__main__':
    ex = GravityCollector('config.json')
    print(ex.retrieve_data(csv=False))
